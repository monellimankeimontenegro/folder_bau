# README #
The project aims to create a Windows folder structure from a Word document. The folder structure has to be structured like the table of contents of the document.

This is achieved by either using the `.exe` 
or the `create_folder_structure_docx.ps1`. 

It is supposed that each heading starts with a **number** and 
and a finishing **.**. Example: **1.2. Subtitle number 2**

## Usage script ##
./create_folder_structure_docx.ps1

### Example ###

1. Switch into working directory of script: `cd C:\Users\<USERNAME>\<TESTFOLDERSTRUCTURE>`

2. Command: `./create_folder_structure_docx.ps1`

## Usage executable ##
To use a GUI instead of scripts, download the executable
- http://downloads.monellimankeimontenegro.eu/folderBau.exe

## Build executable ##
ps2exe is a tool to build a GUI from a PowerShell script
- https://gallery.technet.microsoft.com/scriptcenter/PS2EXE-GUI-Convert-e7cb69d5

## Unit tests ##
- `Invoke-Pester -Script .\Is-Folder-In-Directory.Tests.ps1`
- `Invoke-Pester -Script .\Folder-Utils.Tests.ps1`
- `.\integrationTest.ps1`