function Is-String-In-String {
    # Check if string is part of another string
    Param ([string]$subStr, [string]$bigStr)

    if ($bigStr -Match $subStr) {
        $isExisting = $True
    } else {
        $isExisting = $False
    }

    return $isExisting
}

function Is-String-In-Stringlist {
    # Check if a string is part of another string in a list of strings
    Param ([string]$search, [array]$stringList)

    # Get folder name without prefix numbers
    $folderNameParts = $search.Split(".")

    $isExisting = $False
    ForEach ($string in $stringList) {
        $isExisting = Is-String-In-String -subStr $folderNameParts[-1] -bigStr $string
        if ($isExisting -And (-Not ($search -eq $string))) {
            break
        }
    }

    return $isExisting
}

function Get-String-In-Stringlist {
    # Check if a string is part of another string in a list of strings
    Param ([string]$search, [array]$stringList)

    $existingString = ''
    ForEach ($string in $stringList) {
        $isExisting = Is-String-In-String -subStr $search -bigStr $string
        if ($isExisting) {
            $existingString = $string
            break
        }
    }

    return $existingString
}

function Is-Folder-In-Directory {
    # Check if given folder path with number X.X. <Folder name> already exists
    # but with a different number X.Y. <Folder name>
    Param ([string]$f)
  
    # Get folder name and parent directory from input string
    $folderParentDirectory = Split-Path -Parent $f
    $folderName = Split-Path -Leaf $f

    # Check if parent directory exists
    if (Test-Path $folderParentDirectory -PathType Any) {
        # Get other folders within path_name
        $subfolderList = Get-ChildItem $folderParentDirectory -directory
  
        # Is folder name in other folders' names
        $is_existing = Is-String-In-Stringlist -search $folderName -stringList $subfolderList
        
    } else {
        $is_existing = $False
    }
  
    return $is_existing
}

function Get-Previous-Folder-In-Directory {
    Param ([string]$f)
  
    # Get folder name and parent directory from input string
    $folderParentDirectory = Split-Path -Parent $f
    $folderName = Split-Path -Leaf $f

    # Get folder name without prefix numbers
    $folderNameParts = $folderName.Split(".")

    # Check if parent directory exists
    if (Test-Path $folderParentDirectory -PathType Any) {
        # Get other folders within path_name
        $subfolderList = Get-ChildItem $folderParentDirectory -directory
  
        # Is folder name in other folders' names
        $previousPath = Get-String-In-Stringlist -search $folderNameParts[-1] -stringList $subfolderList
        
    } else {
        $previousPath = ''
    }

    $previousPath = $folderParentDirectory + '\' + $previousPath

    return $previousPath
}
