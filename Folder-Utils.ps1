Import-module .\Is-Folder-In-Directory.ps1 -Force

function Get-ItemDepth {
    # Get the depth of a given item
    # Parameter: Name of the potential folder
    #   <X.X.X. ... Folder name> where X is a number
    Param ([string]$f)

    $currentItem = $f.Split(".")
    $currentDepth = $currentItem.Length

    return $currentDepth
}

function Create-Folder {
    # Create a new folder at the given path
    Param ([string]$f)
  
    # Check if path already exists
    if (Test-Path $f -PathType Any) {
        Write-Host "Folder already exists"
    } else {
      # Check if item number changed to copy old content into new folder
      if (Is-Folder-In-Directory $f) {
        # Create new folder if item does not exist
        New-Item -Path $f -ItemType Directory

        # Get name of previous folder in directory
        Write-Host "Copy previous folder into new folder"
        $previous_f = Get-Previous-Folder-In-Directory $f

        # Copy content into new folder
        # Copy-item -Force -Recurse -Verbose $previous_f -Destination $f
  
        # Delete old folder
        Write-Host "Delete previous folder"
        # Write-Host $previous_f
        Remove-Item -Recurse $previous_f
      } else {
        # Create new folder if item does not exist
        New-Item -Path $f -ItemType Directory
      }
  
    }
}

function Has-Dots {
    # Check if a string has dots
    Param ([string]$phrase)

    # Get number of dots after first symbols
    $dotBlocks = $phrase.Split('.')
    If ($dotBlocks.Length -lt 2) {
        $dotsFlag = $False    # Line has no dots
    } else {
        $dotsFlag = $True
    }

    return $dotsFlag
}

function Is-Heading {
    # Check if a string has heading properties
    Param ([array]$phraseArray)

    If ($phraseArray.Length -lt 2) {
        # Single entry in array with string itself
        $isHeading = $False
    } else {
        $isHeading = $True
    }
    
    return $isHeading
}

function Has-Heading-Length {
    # Check if a string is long enough
    Param ([string]$phrase)

    If([string]::IsNullOrEmpty($phrase)){
        $hasLength = $false
    } elseif ($phrase.Length -lt 2) {
        $hasLength = $False
    } else {
        $hasLength = $True
    }

    return $hasLength
}

function Replace-Numbers-In-String {
    # Replace all numbers in a string and exclude not number values
    Param ([string]$phrase)

    $numberString = $phrase -replace '\D+',''

    return $numberString
}

function Is-Number-In-Phrase {
    # Check if there are numbers in the start string
    Param ([string]$phrase)

    $numbers = Replace-Numbers-In-String -phrase $phrase
    if ($numbers.Length -lt $phrase.Length) {
        $isNumber = $True
    } else {
        $isNumber = $False
    }

    return $isNumber
}

function Is-PhraseFolder {
    # Check if a given phrase is a heading/title
    # Execute primitive phrase checks
    # Heading definition: <Number>.<Number>... <Heading>
    Param ([string]$f)

    # Check basic properties of string for heading phrase
    If (-Not (Has-Heading-Length $f)) {
        return $False
    }
  
    # Check if basic string setup has heading properties
    $phraseBlocks = $f.Split(" ")
    If (-Not (Is-Heading -phraseArray $phraseBlocks)) {
        return $False
    }

    # Check if beginning of phrase has heading properties
    $startPhrase = $phraseBlocks[0]    
    If (-Not (Has-Dots -phrase $startPhrase)) {
        return $False
    }

    If (-Not (Is-Number-In-Phrase -phrase $startPhrase)) {
      return $False
    }
  
    return $True
  }
  
  function Update-BaseDirectory {
    # Change a directory with a new folder in last part until /
    # to make that folder the new base directory
    Param ([string]$base, [string]$folder)
  
    $base = Change-BaseDirectory-By-Depth -base $base -depth 1
    $base += "\" + $folder
  
    return $base
  }
  
  function Change-BaseDirectory-By-Depth {
    # Change the base directorys' by going back some folders
    # Going back depth is set by depth parameter
    Param ([string]$base, [int]$depth)
  
    $base_directory_array = $base.Split('\')
    $newDepth = $base_directory_array.length-$depth

    if ($newDepth -lt 1) {
        Write-Host 'Caution: Going back to global base directory'
        $newDepth = 1
    }

    $base_directory = $base_directory_array[0]
    for ($i=1; $i -lt $newDepth; $i++){
      $base_directory += "\" +  $base_directory_array[$i]
    }
  
    return $base_directory
  }