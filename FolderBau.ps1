Import-module .\Is-Folder-In-Directory.ps1 -Force
Import-module .\Folder-Utils.ps1 -Force

function Build-Folder {
    # Read first parameter as source file name with folder structure definition
    if($args[0]) {
      $fileName = $args[0]
    } else {
      Write-Host "First input argument has to be path to folder definition file"
    }

    # Get current base directory from shell
    if($args[1]){
      $base_directory = $args[1]
    }else{
      $base_directory = (Get-Item -Path ".\").FullName
      Write-Host "No second argument given"
      Write-Host "Using current working directory to create folder structure"
    }

    # Return value: OK, NOK
    $returnValue = $False

    # Get list of folders
    $wordObject = New-Object -ComObject Word.application
    $wordObject.visible = $false
    $documentObject = $wordObject.Documents.Open("$fileName")
    $ArrayEg = $documentObject.Paragraphs

    # Create new folder structure
    $current_depth, $previous_depth = 1, 1
    ForEach ($line_object in $ArrayEg) {
      $folder_name = $line_object.range.Text
  
      if(Is-PhraseFolder -f $folder_name) {
        # Clean folder name by LF
        $folder_name = $folder_name.Substring(0, $folder_name.Length-1)
        $returnValue = $True
      } else {
        continue
      }

      # Check depth of subfolder
      $current_depth = Get-ItemDepth -f $folder_name

      # Check if new folder is deeper than previous
      If($current_depth -gt $previous_depth) {
        # Enter previous folder by updating base directory
        $base_directory += "\" + $folder_name
        $previous_depth = $current_depth

        # Create folder with given name
        Create-Folder -f $base_directory

        continue

      } elseif ($current_depth -lt $previous_depth) {
        # Calculate subfolder depth to which switch back
        $diff_depth = $previous_depth - $current_depth
        $previous_depth -= $diff_depth

        # Return to previous folder as new base directory
        $base_directory = Change-BaseDirectory-By-Depth -base $base_directory -depth $diff_depth
        $base_directory = Update-BaseDirectory -base $base_directory -folder $folder_name

      } else {
        $base_directory = Update-BaseDirectory -base $base_directory -folder $folder_name
      }

      # Create folder with given name
      Create-Folder -f $base_directory
    }

    $wordObject.Quit()

    return $returnValue
}