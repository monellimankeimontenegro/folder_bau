$cwd = Split-Path -Parent $MyInvocation.MyCommand.Path
$scriptUnderTest = (Split-Path -Leaf $MyInvocation.MyCommand.Path).Replace(".Tests.", ".")
. "$cwd\$scriptUnderTest"


describe 'Is-Folder-In-Directory' {
  $test_folder_exist = 'D:\01_code\3_projekte\02_Informatik\bitbucket\folder_bau\example\2. Ueberschrift_2\2.2. Test_2_2'
  $test_folder_not_exist = 'D:\01_code\3_projekte\02_Informatik\bitbucket\folder_bau\example\2. Ueberschrift_2\2.5. Test_2_5'
  $test_folder_exist_different_number = 'D:\01_code\3_projekte\02_Informatik\bitbucket\folder_bau\example\2. Ueberschrift_2\2.5. Test_2_2'

  New-Item -Path $test_folder_exist -ItemType Directory
  New-Item -Path $test_folder_exist_different_number -ItemType Directory

  it 'should return True if the path exists in the parent directory' {
    Is-Folder-In-Directory $test_folder_exist | Should Be $True
  }

  it 'should return False if the path does not in the parent directory' {
    Is-Folder-In-Directory $test_folder_not_exist | Should Be $False
  }

  it 'should return True if the path does not in the parent directory but the name of the folder has different initial numbers' {
    Is-Folder-In-Directory $test_folder_exist_different_number | Should Be $True
  }

  Remove-Item -Recurse 'D:\01_code\3_projekte\02_Informatik\bitbucket\folder_bau\example'
}

describe 'Is-String-In-String' {
  $bigString = '1.1. Test1'
  $partString = 'Test1'

  it 'should return True if string is part of another string' {
    Is-String-In-String -subStr $partString -bigStr $bigString | Should Be $True
  }

  $bigString = '1.1. Test_45'
  $partString = 'Test_asdf'

  it 'should return False if string is part of another string' {
    Is-String-In-String -subStr $partString -bigStr $bigString | Should Be $False
  }
}

describe 'Is-String-In-Stringlist' {
  $testString = 'Test1'
  $testArray = 'Test2', 'Test3', 'Test1'

  it 'should return True if string is part of array of strings' {
    Is-String-In-Stringlist -search $testString -stringList $testArray | Should Be $True
  }

  $testString = 'Test1'
  $testArray = 'Test2', 'Test3', 'Test4'

  it 'should return False if string is not part of array of strings' {
    Is-String-In-Stringlist -search $testString -stringList $testArray | Should Be $False
  }
}

describe 'Get-String-In-Stringlist' {
  $testString = 'Test1'
  $testArray = 'Test2', 'Test3', 'Test1'

  it 'should return True if string is part of array of strings' {
    Get-String-In-Stringlist -search $testString -stringList $testArray | Should Be $testArray[2]
  }
}