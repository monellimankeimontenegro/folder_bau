Import-module .\FolderBau.ps1 -Force

# Test
$cwd = Split-Path -Parent $MyInvocation.MyCommand.Path
$docxFilePath = $cwd + '\folder_structure.docx'
$targetPath = $cwd + '\example'

Build-Folder $docxFilePath $targetPath

# Updated .docx file
$docxFilePath = $cwd + '\folder_structure_2.docx'

Build-Folder $docxFilePath $targetPath

# Remove docx folders
Remove-Item -Recurse $targetPath