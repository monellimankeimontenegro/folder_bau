; folderbau_installer.nsi
;
;--------------------------------

; The name of the installer
Name "FolderBau"

; The file to write
OutFile "folderbau.exe"

; Show creation details
ShowInstDetails show

; Request application privileges for Windows Vista and higher
RequestExecutionLevel admin

; Build Unicode installer
Unicode True

; The default installation directory
InstallDir $PROGRAMFILES\FolderBau

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\FolderBau" "Install_Dir"

;--------------------------------

; Pages during installation

Page components
Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

; The stuff to install
Section "FolderBau"

  SectionIn RO
  
  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  
  ; Put file there
  File "D:\01_code\3_projekte\02_Informatik\bitbucket\folder_bau\create_folder_structure_docx.ps1"
  File "D:\01_code\3_projekte\02_Informatik\bitbucket\folder_bau\FolderBau.ps1"
  File "D:\01_code\3_projekte\02_Informatik\bitbucket\folder_bau\Folder-Utils.ps1"
  File "D:\01_code\3_projekte\02_Informatik\bitbucket\folder_bau\folder_bau.txt"
  File "D:\01_code\3_projekte\02_Informatik\bitbucket\folder_bau\Is-Folder-In-Directory.ps1"
  File /r "D:\01_code\3_projekte\02_Informatik\bitbucket\folder_bau\FolderBau\*"

  
  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\FolderBau "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\FolderBau" "DisplayName" "FolderBau"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\FolderBau" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\FolderBau" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\FolderBau" "NoRepair" 1
  WriteUninstaller "$INSTDIR\uninstall.exe"
  
SectionEnd

; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"

  CreateDirectory "$SMPROGRAMS\FolderBau"
  CreateShortcut "$SMPROGRAMS\FolderBau\Uninstall.lnk" "$INSTDIR\uninstall.exe"
  CreateShortcut "$SMPROGRAMS\FolderBau\FolderBau.lnk" "powershell.exe" "-windowstyle hidden -noexit -ExecutionPolicy Bypass -File $\"$INSTDIR\create_folder_structure_docx.ps1$\""

SectionEnd

;--------------------------------

; Uninstaller

Section "Uninstall"
  
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\FolderBau"
  DeleteRegKey HKLM SOFTWARE\FolderBau

  ; Remove files and uninstaller
  Delete $INSTDIR\folderbau_installer.nsi
  Delete $INSTDIR\uninstall.exe

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\FolderBau\*.lnk"

  ; Remove directories
  RMDir "$SMPROGRAMS\FolderBau"
  RMDir "$INSTDIR"

SectionEnd
