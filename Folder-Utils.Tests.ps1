$cwd = Split-Path -Parent $MyInvocation.MyCommand.Path
$scriptUnderTest = (Split-Path -Leaf $MyInvocation.MyCommand.Path).Replace(".Tests.", ".")
. "$cwd\$scriptUnderTest"


describe 'Get-ItemDepth' {
  $test_folder = '2.2. Test_2_2'
  $test_depth = 3

  it 'should return 2 for a title with 2 dots in the beginning' {
    Get-ItemDepth $test_folder | Should Be $test_depth
  }

  $test_folder = '2 Test_2_2'
  $test_depth = 1

  it 'should return 0 for a title without dots in the beginning' {
    Get-ItemDepth $test_folder | Should Be $test_depth
  }
}

describe 'Change-BaseDirectory-By-Depth' {
  $testPath = 'D:\01_code\3_projekte\02_Informatik\bitbucket\folder_bau\example\2. Ueberschrift_2\2.2. Test_2_2'
  $newPath = 'D:\01_code\3_projekte\02_Informatik\bitbucket\folder_bau'
  $testDepth = 3

  it 'should return path with less depth' {
    Change-BaseDirectory-By-Depth -base $testPath -depth $testDepth | Should Be $newPath
  }

  $testDepth = 100
  it 'should return global base directory' {
    Change-BaseDirectory-By-Depth -base $testPath -depth $testDepth  | Should Be 'D:'
  }
}

describe 'Update-BaseDirectory' {
  $testPath = 'D:\01_code\3_projekte\02_Informatik\bitbucket\folder_bau\example\2. Ueberschrift_2\2.2. Test_2_2'
  $folder = 'new Base'
  $newPath = 'D:\01_code\3_projekte\02_Informatik\bitbucket\folder_bau\example\2. Ueberschrift_2\new Base'

  it 'should change latest folder' {
    Update-BaseDirectory -base $testPath -folder $folder | Should Be $newPath
  }
}
describe 'Has-Dots' {
  $testPath = '2.2.'

  it 'should return True since phrase starts with numbers separated by dots' {
    Has-Dots -phrase $testPath | Should Be $True
  }

  $testPath = '2'
  it 'should return False since there phrase does not start with numbers and dots' {
    Has-Dots -phrase $testPath | Should Be $False
  }
}

describe 'Is-Heading' {
  $testPathArray = '2.2.', 'Test_2_2'

  it 'should return True since array contains more than one element' {
    Is-Heading -phraseArray $testPathArray | Should Be $True
  }

  $testPathArray = 'Test_2_2'
  it 'should return False there is a phrase with a single element' {
    Is-Heading -phraseArray $testPathArray | Should Be $False
  }
}

describe 'Has-Heading-Length' {
  $testPath = '2.2. Test'

  it 'should return True phrase start with numbers and dots and a following string' {
    Has-Heading-Length -phrase $testPath | Should Be $True
  }

  $testPath = 'T'
  it 'should return False phrase is too short' {
    Has-Heading-Length -phrase $testPath | Should Be $False
  }
}

describe 'Replace-Numbers-In-String' {
  $testString = '2.2.3.4.5.'

  it 'should return number in phrase which starts with numbers and dots and a following string' {
    Replace-Numbers-In-String -phrase $testString | Should Be "22345"
  }
}

describe 'Is-Number-In-Phrase' {
  $testString = '2.2.3.4.5.'

  it 'should return True phrase start with numbers and dots and a following string' {
    Is-Number-In-Phrase -phrase $testString | Should Be $True
  }
}

describe 'Is-PhraseFolder' {
  $testString = '2.2.3.4.5. Testüberschrift'

  it 'should return True phrase start with numbers and dots and a following string' {
    Is-PhraseFolder -f $testString | Should Be $True
  }
}
