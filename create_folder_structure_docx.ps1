# This script sets up a graphical interface for the PowerShell script
# in the back

Write-Host 'Create folder structure using Windows PowerShell!'
Add-Type -AssemblyName PresentationFramework
Add-Type -Assembly System.Windows.forms

# Load custom functions from scripts
Import-module .\FolderBau.ps1 -Force

function Select-FolderDialog 
{
  param([String]$Description="Select Folder", 
        [String]$RootFolder="Desktop",
        [System.Windows.Controls.TextBox]$TextOutput)   

  $objForm = New-Object System.Windows.Forms.FolderBrowserDialog
  $objForm.Rootfolder = $RootFolder
  $objForm.Description = $Description
  $Show = $objForm.ShowDialog()
  if ($Show -eq "OK")
  {
     $TextOutput.Text = $objForm.SelectedPath
  }

  # Reset progress bar when folder is choosen
  Reset-ProgressBar -progressBar $var_pgbTarget -TextResult $var_txtResult
}

function Select-FileDialog 
{
  param([System.Windows.Controls.TextBox]$TextOutput)   

  $objForm = New-Object System.Windows.Forms.OpenFileDialog
  $Show = $objForm.ShowDialog()
  if ($Show -eq "OK")
  {
     $TextOutput.Text = $objForm.FileNames
  }
  # Reset progress bar when folder is choosen
  Reset-ProgressBar -progressBar $var_pgbTarget -TextResult $var_txtResult
}

function Write-Title
{
    param([System.Windows.Controls.TextBox]$TitleWindow)

    # Get current working directory
    $currentWorkingDirectory = (Get-Item -Path ".\").FullName

    # Offset for title
    for ($i=1; $i -le 4; $i++){
        $TitleWindow.Text += "`r`n"
    }

    # Get list of lines from title text file
    $file_name = $currentWorkingDirectory + "\folder_bau.txt"
    Get-Content $file_name | ForEach-Object{
        $TitleWindow.Text += $_
        $TitleWindow.Text += "`r`n"
    }
}

function Get-Window
{
    param([XML]$xaml)

    $reader = (New-Object System.Xml.XmlNodeReader $xaml)
    try {
        $window = [Windows.Markup.XamlReader]::Load( $reader )
    } catch {
        Write-Warning $_.Exception
        throw
    }

    return $window
}

function Start-FolderBuild
{   
    # Not working properly in single thread
    Start-ProgressBar -progressBar $var_pgbTarget

    $result = Build-Folder $var_txtFile.Text $var_txtTarget.Text

    if ($result = $True) {
        # Start-Sleep -s 5
        Write-Host "Finished successfully"
        Stop-ProgressBar -progressBar $var_pgbTarget -TextResult $var_txtResult
    } else {
        Write-Host "Finished unsucessfully"
        Stop-ProgressBar -progressBar $var_pgbTarget -TextResult $var_txtResult
    }

}

function Start-ProgressBar
{
    param([System.Windows.Controls.ProgressBar]$progressBar)
    $progressBar.IsIndeterminate = $True
}

function Stop-ProgressBar
{
    param([System.Windows.Controls.ProgressBar]$progressBar,
          [boolean]$okFlag,
          [System.Windows.Controls.TextBox]$TextResult)
    $progressBar.IsIndeterminate = $False
    $progressBar.Value = 100
    if($okFlag = $True){
        $TextResult.Text = "OK"
        $TextResult.Background = "Green"
    } else {
        $TextResult.Text = "Error"
    }
}

function Reset-ProgressBar
{
    param([System.Windows.Controls.ProgressBar]$progressBar,
          [System.Windows.Controls.TextBox]$TextResult)

    $progressBar.IsIndeterminate = $False
    $progressBar.Value = 0

    $TextResult.Text = ""
    $TextResult.Background = "White"
}

# Where is the XAML file
$currentWorkingDirectory = (Get-Item -Path ".\").FullName
$xamlFile = $currentWorkingDirectory + "\FolderBau\MainWindow.xaml"

# Get xaml file with interface description
$inputXML = Get-Content $xamlFile -Raw
$inputXML = $inputXML -replace 'mc:Ignorable="d"', '' -replace "x:N", 'N' -replace '^<Win.*', '<Window'
[XML]$XAML = $inputXML

# Read XAML and create window from it
$window = Get-Window -xaml $XAML

# Create variables based on form control names.
# Variable will be named as 'var_<control name>'

$xaml.SelectNodes("//*[@Name]") | ForEach-Object {
    #"trying item $($_.Name)"
    try {
        Set-Variable -Name "var_$($_.Name)" -Value $window.FindName($_.Name) -ErrorAction Stop
    } catch {
        throw
    }
}
Get-Variable var_*

# Write title
Write-Title -TitleWindow $var_txtTitleWindow

# Add search windows to selection buttons
$var_btnTarget.Add_Click({Select-FolderDialog -TextOutput $var_txtTarget})
$var_btnFile.Add_Click({Select-FileDialog -TextOutput $var_txtFile})

$var_btnCreate.Add_Click({Start-FolderBuild})

$Null = $window.ShowDialog()
