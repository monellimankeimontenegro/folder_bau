Write-Host 'Create folder structure using beloved Windows PowerShell!'

# Read first parameter as source file name with folder structure definition
if($args[0]) {
  $file_name = $args[0] # ".\folder_structure.txt"
} else {
  Write-Host "First input argument has to be path to folder definition file"
}

# Get current base directory from shell
if($args[1]){
  $base_directory = $args[1]
}else{
  $base_directory = (Get-Item -Path ".\").FullName
  Write-Host "No second argument given"
  Write-Host "Using current working directory to create folder structure"
}

# Get list of folders
$ArrayEg = @()
Get-Content $file_name | ForEach-Object{
  $ArrayEg += $_
}

# Delete all subfolders
do {
  $dir = Get-ChildItem $base_directory -directory -recurse | Where-Object { (Get-ChildItem $_.fullName).count -eq 0 } | Select-Object -expandproperty FullName
  $dir | Foreach-Object { Remove-Item $_ }
} while ($dir.count -gt 0)

# Create new folder structure
$current_depth = 1
$previous_depth = 1
ForEach ($folder_name in $ArrayEg) {
  # Check depth of subfolder
  $current_item = $folder_name.Split(".")
  $current_depth = $current_item.Length

  # Check if new folder is deeper than previous
  If($current_depth -gt $previous_depth) {
    # Enter previous folder by updating base directory
    $base_directory += "\" + $folder_name
    $previous_depth = $current_depth

    # Create folder with given name
    If([string]::IsNullOrEmpty($folder_name)){
      Write-Host "Warning empty name line"
    }
    Else {
      $folder_path = $base_directory
      Write-Host $folder_path
      New-Item -Path $folder_path -ItemType Directory
    }
    continue
    
  } elseif ($current_depth -lt $previous_depth) {
    $diff_depth = $previous_depth - $current_depth
    $base_directory_array = $base_directory.Split('\')

    $base_directory = $base_directory_array[0]
    for ($i=1; $i -lt $base_directory_array.length-$diff_depth; $i++){
      $base_directory += "\" +  $base_directory_array[$i]
    }
    $previous_depth -= $diff_depth
  } else {
    $base_directory_array = $base_directory.Split('\')

    $base_directory = $base_directory_array[0]
    for ($i=1; $i -lt $base_directory_array.length-1; $i++){
      $base_directory += "\" +  $base_directory_array[$i]
    }
    $base_directory += "\" + $folder_name
  }

  # Create folder with given name
  If([string]::IsNullOrEmpty($folder_name)){
    Write-Host "Warning empty name line"
  }
  Else {
    $base_directory_array = $base_directory.Split('\')
    $folder_path = $base_directory_array[0]
    for ($i=1; $i -lt $base_directory_array.length-1; $i++){
      $folder_path += "\" +  $base_directory_array[$i]
    }

    $folder_path = $folder_path + "\" + $folder_name
    Write-Host $folder_path
    New-Item -Path $folder_path -ItemType Directory
  }
}
